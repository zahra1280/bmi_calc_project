import unittest
from calculate_bmi import calculate_bmi

class TestBMICalculator(unittest.TestCase):

    def test_calculate_bmi_valid_input(self):

        result = calculate_bmi(170, 70)
        self.assertAlmostEqual(result, 24.22, places=2)

if __name__ == '__main__':
    unittest.main()
